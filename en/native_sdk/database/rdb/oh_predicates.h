/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_PREDICATES_H
#define OH_PREDICATES_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief The relational database (RDB) store manages data based on relational models.
 * A complete set of mechanisms for managing local databases is provided based on the underlying SQLite.
 * To satisfy different needs in complicated scenarios, the RDB module provides a series of methods for performing
 * operations such as adding, deleting, modifying, and querying data, and supports direct execution of SQL statements.
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */

/**
 * @file oh_predicates.h
 *
 * @brief Represents a predicate for a relational database (RDB).
 * @library native_rdb_ndk_header.so
 * @since 10
 */

#include <cstdint>
#include <stddef.h>
#include "oh_value_object.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the sorting types.
 *
 * @since 10
 */
typedef enum OH_OrderType {
    /** Ascending order. */
    ASC = 0,
    /** Descending order. */
    DESC = 1,
} OH_OrderType;

/**
 * @brief Defines a <b>Predicates</b> instance.
 *
 * @since 10
 */
typedef struct OH_Predicates {
    /** Unique identifier of the OH_Predicates struct. */
    int64_t id;

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field whose value is equal to the specified value.
     *
     * This method is equivalent to "=" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*equalTo)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field whose value is not equal to the specified value.
     *
     * This method is equivalent to "!=" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*notEqualTo)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Adds a left parenthesis to the <b>Predicates</b> instance.
     *
     * This method is equivalent to "(" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns a <b>Predicates</b> instance with a left parenthesis.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*beginWrap)(OH_Predicates *predicates);

    /**
     * @brief Adds a right parenthesis to the <b>Predicates</b> instance.
     *
     * This method is equivalent to ")" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns a <b>Predicates</b> instance with a right parenthesis.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*endWrap)(OH_Predicates *predicates);

    /**
     * @brief Adds the OR operator to the <b>Predicates</b> instance.
     *
     * This method is equivalent to "OR" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns a <b>Predicates</b> instance with the OR operator.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*orOperate)(OH_Predicates *predicates);

    /**
     * @brief Adds the AND operator to the <b>Predicates</b> instance.
     *
     * This method is equivalent to "AND" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns a <b>Predicates</b> instance with the AND operator.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*andOperate)(OH_Predicates *predicates);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field whose value is null.
     *
     * This method is equivalent to "IS NULL" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*isNull)(OH_Predicates *predicates, const char *field);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field whose value is not null.
     *
     * This method is equivalent to "IS NOT NULL" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*isNotNull)(OH_Predicates *predicates, const char *field);

    /**
     * @brief Sets a <b>Predicates</b> instance to match a string that is similar to the specified value.
     *
     * This method is equivalent to "LIKE" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*like)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field whose value is within the specified range.
     *
     * This method is equivalent to "BETWEEN" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value range to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*between)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field whose value is out of the specified range.
     *
     * This method is equivalent to "NOT BETWEEN" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the range to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*notBetween)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field with value greater than the specified value.
     *
     * This method is equivalent to ">" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*greaterThan)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field with value less than the specified value.
     *
     * This method is equivalent to "<" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*lessThan)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field with value greater than or equal to
     *  the specified value.
     * This method is equivalent to ">=" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*greaterThanOrEqualTo)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to match the field with value less than or equal to the specified value.
     *
     * This method is equivalent to "<=" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*lessThanOrEqualTo)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> instance to sort the values in a column in ascending or descending order.
     *
     * This method is equivalent to "ORDER BY" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the column name in the database table.
     * @param type Indicates the sorting type {@link OH_OrderType}.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_OrderType.
     * @since 10
     */
    OH_Predicates *(*orderBy)(OH_Predicates *predicates, const char *field, OH_OrderType type);

    /**
     * @brief Sets a <b>Predicates</b> instance to filter out duplicate records.
     *
     * This method is equivalent to "DISTINCT" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns a <b>Predicates</b> instance that can filter duplicate records.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*distinct)(OH_Predicates *predicates);

    /**
     * @brief Sets a <b>Predicates</b> instance that specifies the maximum number of records.
     *
     * This method is equivalent to "LIMIT" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param value Indicates the maximum number of data records.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*limit)(OH_Predicates *predicates, unsigned int value);

    /**
     * @brief Sets a <b>Predicates</b> instance that specifies the start position of the returned result.
     *
     * This method is equivalent to "OFFSET" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param rowOffset Indicates the start position of the returned result. The value is a positive integer.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates.
     * @since 10
     * @version 1.0
     */
    OH_Predicates *(*offset)(OH_Predicates *predicates, unsigned int rowOffset);

    /**
     * @brief Sets a <b>Predicates</b> instance to group rows that have the same value into summary rows.
     *
     * This method is equivalent to "GROUP BY" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param fields Indicates the pointer to the columns to group.
     * @param length Indicates the length of the <b>fields</b> value.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates.
     * @since 10
     */
    OH_Predicates *(*groupBy)(OH_Predicates *predicates, char const *const *fields, int length);

    /**
     * @brief Sets a <b>Predicates</b> to match the field with the value within the specified range.
     *
     * This method is equivalent to "IN" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the name of the column in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     */
    OH_Predicates *(*in)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Sets a <b>Predicates</b> to match the field with the value out of the specified range.
     *
     * This method is equivalent to "NOT IN" in SQL statements.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @param field Indicates the pointer to the name of the column in the database table.
     * @param valueObject Indicates the pointer to the {@link OH_VObject} instance, which is the value to match.
     * @return Returns the <b>Predicates</b> instance created.
     * @see OH_Predicates, OH_VObject.
     * @since 10
     * @version 1.0
     */
    OH_Predicates *(*notIn)(OH_Predicates *predicates, const char *field, OH_VObject *valueObject);

    /**
     * @brief Clears a <b>Predicates</b> instance.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns the <b>Predicates</b> instance cleared.
     * @see OH_Predicates.
     * @since 10
     * @version 1.0
     */
    OH_Predicates *(*clear)(OH_Predicates *predicates);

    /**
     * @brief Destroys a {@link OH_Predicates} instance and reclaims the memory occupied.
     *
     * @param predicates Indicates the pointer to the {@link OH_Predicates} instance.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Predicates.
     * @since 10
     */
    int (*destroy)(OH_Predicates *predicates);
} OH_Predicates;

#ifdef __cplusplus
};
#endif

#endif // OH_PREDICATES_H
