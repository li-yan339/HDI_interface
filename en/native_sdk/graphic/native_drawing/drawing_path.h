/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PATH_H
#define C_INCLUDE_DRAWING_PATH_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_path.h
 *
 * @brief Declares the functions related to the path in the drawing module.
 *
 File to include: native_drawing/drawing_path.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the directions of a closed contour.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathDirection {
    /** Adds a closed contour clockwise. */
    PATH_DIRECTION_CW,
    /** Adds a closed contour counterclockwise. */
    PATH_DIRECTION_CCW,
} OH_Drawing_PathDirection;

/**
 * @brief Enumerates the fill types of a path.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathFillType {
    /** Draws inside of the area surrounded by all line segments. */
    PATH_FILL_TYPE_WINDING,
    /** Draws inside of the area surrounded by all line segments by odd times. */
    PATH_FILL_TYPE_EVEN_ODD,
    /** Same as <b>PATH_FILL_TYPE_WINDING</b>, but draws outside of the area surrounded by all line segments. */
    PATH_FILL_TYPE_INVERSE_WINDING,
    /**
     * Same as <b>PATH_FILL_TYPE_EVEN_ODD</b>, but draws outside of the area surrounded by all line segments
     * by odd times.
     */
    PATH_FILL_TYPE_INVERSE_EVEN_ODD,
} OH_Drawing_PathFillType;

/**
 * @brief Creates an <b>OH_Drawing_Path</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Path</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCreate(void);

/**
 * @brief Copies an existing {@link OH_Drawing_Path} object to create a new one.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @return Returns the pointer to the {@link OH_Drawing_Path} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCopy(OH_Drawing_Path*);

/**
 * @brief Destroys an <b>OH_Drawing_Path</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathDestroy(OH_Drawing_Path*);

/**
 * @brief Sets the start point of a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param x X coordinate of the start point.
 * @param y Y coordinate of the start point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief Draws a line segment from the last point of a path to the target point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param x X coordinate of the target point.
 * @param y Y coordinate of the target point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief Draws an arc to a path. This is done by using angle arc mode. In this mode, a rectangle that encloses an
 * ellipse is specified first, and then a start angle and a sweep angle are specified. The arc is a portion of the
 * ellipse defined by the start angle and the sweep angle.
 * By default, a line segment from the last point of the path to the start point of the arc is also added.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param x1 X coordinate of the upper left corner of the rectangle.
 * @param y1 Y coordinate of the upper left corner of the rectangle.
 * @param x2 X coordinate of the lower right corner of the rectangle.
 * @param y2 Y coordinate of the lower right corner of the rectangle.
 * @param startDeg Start angle.
 * @param sweepDeg Sweep degree.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathArcTo(OH_Drawing_Path*, float x1, float y1, float x2, float y2, float startDeg, float sweepDeg);

/**
 * @brief Draws a quadratic Bezier curve from the last point of a path to the target point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param ctrlX X coordinate of the control point.
 * @param ctrlY Y coordinate of the control point.
 * @param endX X coordinate of the target point.
 * @param endY Y coordinate of the target point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief Draws a cubic Bezier curve from the last point of a path to the target point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param ctrlX1 X coordinate of the first control point.
 * @param ctrlY1 Y coordinate of the first control point.
 * @param ctrlX2 X coordinate of the second control point.
 * @param ctrlY2 Y coordinate of the second control point.
 * @param endX X coordinate of the target point.
 * @param endY Y coordinate of the target point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief Adds a rectangle contour to a path in the specified direction.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param left X coordinate of the upper left corner of the rectangle.
 * @param top Y coordinate of the upper left corner of the rectangle.
 * @param right X coordinate of the lower right corner of the rectangle.
 * @param bottom Y coordinate of the lower right corner of the rectangle.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRect(OH_Drawing_Path*, float left, float top,
    float right, float bottom, OH_Drawing_PathDirection);

/**
 * @brief Adds a rounded rectangle contour to a path in the specified direction.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_RoundRect Pointer to an {@link OH_Drawing_RoundRect} object.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRoundRect(OH_Drawing_Path*, const OH_Drawing_RoundRect* roundRect, OH_Drawing_PathDirection);

/**
 * @brief Adds an arc to a path as the start of a new contour. The arc added is part of the ellipse bounded by oval,
 * from the start angle through the sweep angle, measured in degrees. A positive angle indicates a clockwise sweep,
 * and a negative angle indicates a counterclockwise sweep. If the sweep angle is less than or equal to -360°,
 * or if the sweep angle is greater than or equal to 360° and start angle modulo 90 is nearly zero,
 * an oval instead of an ellipse is added.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param startAngle Start angle of the arc, in degrees.
 * @param sweepAngle Angle to sweep, in degrees. A positive number indicates a clockwise sweep,
 * and a negative number indicates a counterclockwise sweep.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddArc(OH_Drawing_Path*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief Adds a copy of src to the path, transformed by matrix.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param src Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * If null is passed in, it is the identity matrix.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPath(OH_Drawing_Path*, const OH_Drawing_Path* src, const OH_Drawing_Matrix*);

/**
 * @brief Checks whether a coordinate point is included in a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param x Coordinate point on the X axis.
 * @param y Coordinate point on the Y axis.
 * @return Returns <b>true</b> if the coordinate point is included in the path; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathContains(OH_Drawing_Path*, float x, float y);

/**
 * @brief Transforms the points in a path by matrix.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathTransform(OH_Drawing_Path*, const OH_Drawing_Matrix*);

/**
 * @brief Sets the fill type for a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_PathFillType Fill type of the path.
 * For details about the available options, see {@link OH_Drawing_PathFillType}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathSetFillType(OH_Drawing_Path*, OH_Drawing_PathFillType);


/**
 * @brief Closes a path by drawing a line segment from the current point to the start point of the path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathClose(OH_Drawing_Path*);

/**
 * @brief Resets path data.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathReset(OH_Drawing_Path*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
