/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief Defines driver interfaces of the display module.
 *
 * This module provides driver interfaces for upper-layer graphics services, including layer management, device control, and display buffer management.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IMapperInterface.idl
 *
 * @brief Declares the memory mapping interfaces.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the display module interfaces.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.buffer.v1_0;

import ohos.hdi.display.buffer.v1_0.DisplayBufferType;

sequenceable OHOS.HDI.Display.BufferHandleParcelable;

 /**
 * @brief Defines the class that provides memory mapping interfaces.
 *
 * This class provides interfaces for releasing the display memory, mapping the display memory, and mapping the YUV memory.
 *
 * @since 3.2
 * @version 1.0
 */

interface IMapperInterface {
    /**
     * @brief Releases the display memory.
     *
     * @param handle Indicates the pointer to the handle to the memory to release.
     *
     * @return Returns a valid address if the operation is successful; returns <b>NULL</b> otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    FreeMem([in] BufferHandleParcelable handle);

    /**
     * @brief Maps memory to memory without cache in the process's address space.
     *
     * @param handle Indicates the pointer to the handle to the memory to be mapped.
     *
     * @return Returns a valid address if the operation is successful; returns <b>NULL</b> otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    Mmap([in] BufferHandleParcelable handle);

    /**
     * @brief Maps YUV memory.
     *
     * @param handle Indicates the pointer to the handle to the memory to be mapped.
     *
     * @return Returns a valid address if the operation is successful; returns <b>NULL</b> otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    MmapCache([in] BufferHandleParcelable buffer);

    /**
     * @brief Unmaps memory, that is, removes any mappings in the process's address space.
     *
     * @param handle Indicates the pointer to the handle to the memory to be unmapped.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    Unmap([in] BufferHandleParcelable handle);

    /**
     * @brief Flushes data from the cache to memory and invalidates the data in the cache.
     *
     * @param handle Indicates the pointer to the handle to the cache to be flushed.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    FlushCache([in] BufferHandleParcelable handle);

    /**
     * @brief Flushes data from the cache mapped via {@link Mmap} to memory and invalidates the data in the cache.
     *
     * @param handle Indicates the pointer to the handle to the cache to be flushed.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    FlushMCache([in] BufferHandleParcelable buffer);

    /**
     * @brief Invalidates the data in the cache to store the updated memory content.
     *
     * @param handle Indicates the pointer to the handle to the cache where data is to be invalidated.
     *
     * @return Returns <b>DISPLAY_SUCCESS</b> if the operation is successful.
     * @return Returns an error code defined in {@link DispErrCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    InvalidateCache([in] BufferHandleParcelable handle);
}
/** @} */
