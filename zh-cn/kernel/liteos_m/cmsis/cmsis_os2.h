/*
 * Copyright (c) 2013-2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ----------------------------------------------------------------------
 *
 * $Date:        12. June 2020
 * $Revision:    V2.1.3
 *
 * Project:      CMSIS-RTOS2 API
 * Title:        cmsis_os2.h header file
 *
 * Version 2.1.3
 *    Additional functions allowed to be called from Interrupt Service Routines:
 *    - osThreadGetId
 * Version 2.1.2
 *    Additional functions allowed to be called from Interrupt Service Routines:
 *    - osKernelGetInfo, osKernelGetState
 * Version 2.1.1
 *    Additional functions allowed to be called from Interrupt Service Routines:
 *    - osKernelGetTickCount, osKernelGetTickFreq
 *    Changed Kernel Tick type to uint32_t:
 *    - updated: osKernelGetTickCount, osDelayUntil
 * Version 2.1.0
 *    Support for critical and uncritical sections (nesting safe):
 *    - updated: osKernelLock, osKernelUnlock
 *    - added: osKernelRestoreLock
 *    Updated Thread and Event Flags:
 *    - changed flags parameter and return type from int32_t to uint32_t
 * Version 2.0.0
 *    Initial Release
 *---------------------------------------------------------------------------*/

/**
* @addtogroup CMSIS-RTOS
* @{
*
* @brief CMSIS 2.0 标准接口。
*
* Arm Cortex-M系列处理器的通用的RTOS接口，为需要RTOS功能的软件组件提供了标准化的API。
*
* @since 1.0
*/

/**
* @file cmsis_os2.h
*
* @brief CMSIS 2.0 标准接口头文件。
*
* 该头文件用于对外提供cmsis 2.0标准接口， \n
* 包括内核，线程，定时器，事件，内存，消息队列，互斥锁，信号量等模块及对应的结构体，宏定义。 \n
*
*
* @since 1.0
*/
#ifndef CMSIS_OS2_H_
#define CMSIS_OS2_H_

#ifndef __NO_RETURN
#if   defined(__CC_ARM)
#define __NO_RETURN __declspec(noreturn)
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
#define __NO_RETURN __attribute__((__noreturn__))
#elif defined(__GNUC__)
#define __NO_RETURN __attribute__((__noreturn__))
#elif defined(__ICCARM__)
#define __NO_RETURN __noreturn
#else
#define __NO_RETURN
#endif
#endif

#include <stdint.h>
#include <stddef.h>

#ifdef  __cplusplus
extern "C"
{
#endif

/**
* @brief 系统版本信息结构体。
*
* 用于描述系统的版本信息。
*
* @since 1.0
*/
typedef struct {
  /** API接口版本号 */
  uint32_t                       api;
  /** 内核版本号 */
  uint32_t                    kernel;
} osVersion_t;

/**
* @brief 内核状态枚举。
*
* 该枚举用于描述内核的运行状态。
*
* @since 1.0
*/
typedef enum {
  /** 内核未启动 */
  osKernelInactive        =  0,
  /** 内核就绪 */
  osKernelReady           =  1,
  /** 内核运行中 */
  osKernelRunning         =  2,
  /** 内核锁定 */
  osKernelLocked          =  3,
  /** 内核挂起 */
  osKernelSuspended       =  4,
  /** 内核异常 */
  osKernelError           = -1,
  /** 保留的状态，用于阻止编译器对枚举进行优化 */
  osKernelReserved        = 0x7FFFFFFF
} osKernelState_t;

/**
* @brief 线程状态枚举。
*
* 该枚举用于描述线程的运行状态。
*
* @since 1.0
*/
typedef enum {
  /** 线程未运行 */
  osThreadInactive        =  0,
  /** 线程就绪 */
  osThreadReady           =  1,
  /** 线程运行中 */
  osThreadRunning         =  2,
  /** 线程阻塞 */
  osThreadBlocked         =  3,
  /** 线程结束 */
  osThreadTerminated      =  4,
  /** 线程异常 */
  osThreadError           = -1,
  /** 保留的状态，用于阻止编译器对枚举进行优化 */
  osThreadReserved        = 0x7FFFFFFF
} osThreadState_t;

/**
* @brief 优先级枚举。
*
* 该枚举用于描述各级别优先级。
*
* @since 1.0
*/
typedef enum {
  /** 未初始化的优先级 */
  osPriorityNone          =  0,
  /** 为空闲线程保留的最低优先级，非空闲线程不能使用该优先级 */
  osPriorityIdle          =  1,
  /** 低优先级，这是非空闲线程当前版本可使用的最低优先级 */
  osPriorityLow           =  8,
  /** 低优先级 + 1 */
  osPriorityLow1          =  8+1,
  /** 低优先级 + 2 */
  osPriorityLow2          =  8+2,
  /** 低优先级 + 3 */
  osPriorityLow3          =  8+3,
  /** 低优先级 + 4 */
  osPriorityLow4          =  8+4,
  /** 低优先级 + 5 */
  osPriorityLow5          =  8+5,
  /** 低优先级 + 6 */
  osPriorityLow6          =  8+6,
  /** 低优先级 + 7 */
  osPriorityLow7          =  8+7,
  /** 比普通等级稍低的优先级 */
  osPriorityBelowNormal   = 16,
  /** 比普通等级稍低的优先级 + 1 */
  osPriorityBelowNormal1  = 16+1,
  /** 比普通等级稍低的优先级 + 2 */
  osPriorityBelowNormal2  = 16+2,
  /** 比普通等级稍低的优先级 + 3 */
  osPriorityBelowNormal3  = 16+3,
  /** 比普通等级稍低的优先级 + 4 */
  osPriorityBelowNormal4  = 16+4,
  /** 比普通等级稍低的优先级 + 5 */
  osPriorityBelowNormal5  = 16+5,
  /** 比普通等级稍低的优先级 + 6 */
  osPriorityBelowNormal6  = 16+6,
  /** 比普通等级稍低的优先级 + 7 */
  osPriorityBelowNormal7  = 16+7,
  /** 普通优先级 */
  osPriorityNormal        = 24,
  /** 普通优先级 + 1 */
  osPriorityNormal1       = 24+1,
  /** 普通优先级 + 2 */
  osPriorityNormal2       = 24+2,
  /** 普通优先级 + 3 */
  osPriorityNormal3       = 24+3,
  /** 普通优先级 + 4 */
  osPriorityNormal4       = 24+4,
  /** 普通优先级 + 5 */
  osPriorityNormal5       = 24+5,
  /** 普通优先级 + 6 */
  osPriorityNormal6       = 24+6,
  /** 普通优先级 + 7 */
  osPriorityNormal7       = 24+7,
  /** 比普通等级稍高的优先级 */
  osPriorityAboveNormal   = 32,
  /** 比普通等级稍高的优先级 + 1，这是当前版本可用的最高优先级 */
  osPriorityAboveNormal1  = 32+1,
  /** 比普通等级稍高的优先级 + 2 */
  osPriorityAboveNormal2  = 32+2,
  /** 比普通等级稍高的优先级 + 3 */
  osPriorityAboveNormal3  = 32+3,
  /** 比普通等级稍高的优先级 + 4 */
  osPriorityAboveNormal4  = 32+4,
  /** 比普通等级稍高的优先级 + 5 */
  osPriorityAboveNormal5  = 32+5,
  /** 比普通等级稍高的优先级 + 6 */
  osPriorityAboveNormal6  = 32+6,
  /** 比普通等级稍高的优先级 + 7 */
  osPriorityAboveNormal7  = 32+7,
  /** 高优先级 */
  osPriorityHigh          = 40,
  /** 高优先级 + 1 */
  osPriorityHigh1         = 40+1,
  /** 高优先级 + 2 */
  osPriorityHigh2         = 40+2,
  /** 高优先级 + 3 */
  osPriorityHigh3         = 40+3,
  /** 高优先级 + 4 */
  osPriorityHigh4         = 40+4,
  /** 高优先级 + 5 */
  osPriorityHigh5         = 40+5,
  /** 高优先级 + 6 */
  osPriorityHigh6         = 40+6,
  /** 高优先级 + 7 */
  osPriorityHigh7         = 40+7,
  /** 实时优先级 */
  osPriorityRealtime      = 48,
  /** 实时优先级 + 1 */
  osPriorityRealtime1     = 48+1,
  /** 实时优先级 + 2 */
  osPriorityRealtime2     = 48+2,
  /** 实时优先级 + 3 */
  osPriorityRealtime3     = 48+3,
  /** 实时优先级 + 4 */
  osPriorityRealtime4     = 48+4,
  /** 实时优先级 + 5 */
  osPriorityRealtime5     = 48+5,
  /** 实时优先级 + 6 */
  osPriorityRealtime6     = 48+6,
  /** 实时优先级 + 7 */
  osPriorityRealtime7     = 48+7,
  /** 为ISR（interrupt service routine，中断服务程序）保留的优先级 */
  osPriorityISR           = 56,
  /** 非法的优先级 */
  osPriorityError         = -1,
  /** 保留的优先级，用于阻止编译器对枚举进行优化 */
  osPriorityReserved      = 0x7FFFFFFF
} osPriority_t;

/**
* @brief 线程入口类型。
*
* 该类型用于线程调度时回调。
*
* @since 1.0
*/
typedef void (*osThreadFunc_t) (void *argument);

/**
* @brief 定时器入口类型。
*
* 该类型用于定时器触发时回调。
*
* @since 1.0
*/
typedef void (*osTimerFunc_t) (void *argument);

/**
* @brief 定时器类型枚举。
*
* 该枚举用于定义定时器的类型。
*
* @since 1.0
*/
typedef enum {
  /** 单次触发定时器 */
  osTimerOnce               = 0,
  /** 重复触发定时器 */
  osTimerPeriodic           = 1
} osTimerType_t;

/**
* @brief 系统等待时间宏定义。
*
* 该宏用于定义系统永久等待，直到资源变得可用。
*
* @since 1.0
*/
#define osWaitForever         0xFFFFFFFFU

/**
* @brief 等待任何标志触发的宏定义
*
* 该宏用于标志相关函数中设置触发的方式。
*
* @since 1.0
*/
#define osFlagsWaitAny        0x00000000U

/**
* @brief 等待全部标志触发的宏定义。
*
* 该宏用于标志相关函数中设置触发的方式。
*
* @since 1.0
*/
#define osFlagsWaitAll        0x00000001U

/**
* @brief 不清除指定等待标志的宏定义。
*
* 该宏用于标志相关函数中设置触发的方式。
*
* @since 1.0
*/
#define osFlagsNoClear        0x00000002U

/**
* @brief 标志相关函数最高位错误的宏定义，暂不支持。
*
* @since 1.0
*/
#define osFlagsError          0x80000000U

/**
* @brief 线程标志未知错误的宏定义。
*
* @since 1.0
*/
#define osFlagsErrorUnknown   0xFFFFFFFFU

/**
* @brief 线程标志超时错误的宏定义。
*
* @since 1.0
*/
#define osFlagsErrorTimeout   0xFFFFFFFEU

/**
* @brief 线程标志资源错误的宏定义。
*
* @since 1.0
*/
#define osFlagsErrorResource  0xFFFFFFFDU

/**
* @brief 线程标志参数错误的宏定义。
*
* @since 1.0
*/
#define osFlagsErrorParameter 0xFFFFFFFCU

/**
* @brief 线程标志中断错误的宏定义。
*
* @since 1.0
*/
#define osFlagsErrorISR       0xFFFFFFFAU

/**
* @brief 表示线程不可加入状态的宏。
*
* 该宏表示无法使用{@link osThreadJoin}加入处于此状态的线程。
*
* @since 1.0
*/
#define osThreadDetached      0x00000000U

/**
* @brief 表示线程可加入状态的宏。
*
* 该宏表示可以使用{@link osThreadJoin}加入处于这种状态的线程。
*
* @since 1.0
*/
#define osThreadJoinable      0x00000001U

/**
* @brief 互斥锁递归属性宏，暂不支持。
*
* @since 1.0
*/
#define osMutexRecursive      0x00000001U

/**
* @brief 互斥锁优先级继承属性宏，暂不支持。
*
* @since 1.0
*/
#define osMutexPrioInherit    0x00000002U

/**
* @brief 互斥锁不自动释放属性宏，暂不支持。
*
* @since 1.0
*/
#define osMutexRobust         0x00000008U

/**
* @brief CMSIS-RTOS标准返回值枚举。
*
* 该枚举用于定义CMSIS-RTOS标准返回值。
*
* @since 1.0
*/
typedef enum {
  /** 操作成功 */
  osOK                      =  0,
  /** 未特别说明的错误 */
  osError                   = -1,
  /** 超时错误 */
  osErrorTimeout            = -2,
  /** 资源错误 */
  osErrorResource           = -3,
  /** 参数错误 */
  osErrorParameter          = -4,
  /** 内存不足 */
  osErrorNoMemory           = -5,
  /** 中断服务错误 */
  osErrorISR                = -6,
  /** 保留的定义，用于阻止编译器对枚举进行优化 */
  osStatusReserved          = 0x7FFFFFFF
} osStatus_t;

/**
* @brief 线程ID类型。
*
* 该类型用于识别不同线程。
*
* @since 1.0
*/
typedef void *osThreadId_t;

/**
* @brief 定时器ID类型。
*
* 该类型用于识别不同定时器。
*
* @since 1.0
*/
typedef void *osTimerId_t;

/**
* @brief 事件ID类型。
*
* 该类型用于识别不同事件。
*
* @since 1.0
*/
typedef void *osEventFlagsId_t;

/**
* @brief 互斥锁ID类型。
*
* 该类型用于识别不同互斥锁。
*
* @since 1.0
*/
typedef void *osMutexId_t;

/**
* @brief 信号量ID类型。
*
* 该类型用于识别不同信号量。
*
* @since 1.0
*/
typedef void *osSemaphoreId_t;

/**
* @brief 内存池ID类型。
*
* 该类型用于识别不同内存池。
*
* @since 1.0
*/
typedef void *osMemoryPoolId_t;

/**
* @brief 消息队列ID类型。
*
* 该类型用于识别不同消息队列。
*
* @since 1.0
*/
typedef void *osMessageQueueId_t;


#ifndef TZ_MODULEID_T
#define TZ_MODULEID_T
/**
* @brief 线程安全模块ID类型，暂不支持。
*
* @since 1.0
*/
typedef uint32_t TZ_ModuleId_t;
#endif

/**
* @brief 线程参数类型。
*
* 该类型用于初始化线程的各项配置，使用限制参考{@link osThreadNew}。
*
* @since 1.0
*/
typedef struct {
  /** 线程名称 */
  const char                   *name;
  /** 线程属性位 */
  uint32_t                 attr_bits;
  /** 线程静态控制块指针 */
  void                      *cb_mem;
  /** 线程静态控制块大小，单位：字节 */
  uint32_t                   cb_size;
  /** 线程栈指针 */
  void                   *stack_mem;
  /** 线程栈大小，单位：字节 */
  uint32_t                stack_size;
  /** 线程优先级 */
  osPriority_t              priority;
  /** 线程安全配置 */
  TZ_ModuleId_t            tz_module;
  /** 线程保留参数 */
  uint32_t                  reserved;
} osThreadAttr_t;

/**
* @brief 定时器参数类型，暂不支持配置。
*
* @since 1.0
*/
typedef struct {
  /** 定时器名称 */
  const char                   *name;
  /** 定时器属性位 */
  uint32_t                 attr_bits;
  /** 定时器控制块指针 */
  void                      *cb_mem;
  /** 定时器控制块大小 */
  uint32_t                   cb_size;
} osTimerAttr_t;

/**
* @brief 事件参数类型，暂不支持配置。
*
* @since 1.0
*/
typedef struct {
  /** 事件名称 */
  const char                   *name;
  /** 事件属性位 */
  uint32_t                 attr_bits;
  /** 事件控制块指针 */
  void                      *cb_mem;
  /** 事件控制块大小 */
  uint32_t                   cb_size;
} osEventFlagsAttr_t;

/**
* @brief 互斥锁参数类型，暂不支持配置。
*
* @since 1.0
*/
typedef struct {
  /** 互斥锁名称 */
  const char                   *name;
  /** 互斥锁属性位 */
  uint32_t                 attr_bits;
  /** 互斥锁控制块指针 */
  void                      *cb_mem;
  /** 互斥锁控制块大小 */
  uint32_t                   cb_size;
} osMutexAttr_t;

/**
* @brief 信号量参数类型，暂不支持配置。
*
* @since 1.0
*/
typedef struct {
  /** 信号量名称 */
  const char                   *name;
  /** 信号量属性位 */
  uint32_t                 attr_bits;
  /** 信号量控制块指针 */
  void                      *cb_mem;
  /** 信号量控制块大小 */
  uint32_t                   cb_size;
} osSemaphoreAttr_t;

/**
* @brief 内存池参数类型。
*
* 该类型用于初始化内存池的各项配置，使用限制参考{@link osMemoryPoolNew}。
*
* @since 1.0
*/
typedef struct {
  /** 内存池名称 */
  const char                   *name;
  /** 内存池属性位 */
  uint32_t                 attr_bits;
  /** 内存池静态控制块指针 */
  void                      *cb_mem;
  /** 内存池静态控制块大小，单位: 字节 */
  uint32_t                   cb_size;
  /** 内存池静态存储空间指针 */
  void                      *mp_mem;
  /** 内存池静态存储空间大小，单位: 字节 */
  uint32_t                   mp_size;
} osMemoryPoolAttr_t;

/**
* @brief 消息队列参数类型。
*
* 该类型用于初始化消息队列的各项配置，使用限制参考{@link osMessageQueueNew}。
*
* @since 1.0
*/
typedef struct {
  /** 消息队列名称 */
  const char                   *name;
  /** 消息队列属性位 */
  uint32_t                 attr_bits;
  /** 消息队列静态控制块指针 */
  void                      *cb_mem;
  /** 消息队列静态控制块大小，单位: 字节 */
  uint32_t                   cb_size;
  /** 消息队列静态内存指针 */
  void                      *mq_mem;
  /** 消息队列静态内存大小，单位: 字节 */
  uint32_t                   mq_size;
} osMessageQueueAttr_t;

/**
* @brief 内核初始化函数。
*
* 初始化实时操作系统内核，大部分接口无法在初始化之前使用。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误则返回{@link osError}。
*
* @since 1.0
*/
osStatus_t osKernelInitialize (void);

/**
* @brief 获取内核信息函数。
*
* 获取系统版本号和系统名称。
*
* @param version 指向获取版本信息缓冲区的指针。
* @param id_buf 指向获取内核版本缓冲区的指针。
* @param id_size 用于获取内核版本缓冲区的大小。
*
* @return 成功返回{@link osOK}
* @return 其他错误返回{@link osError}
*
* @since 1.0
*/
osStatus_t osKernelGetInfo (osVersion_t *version, char *id_buf, uint32_t id_size);

/**
* @brief 获取内核状态。
*
* @return 返回系统状态的枚举值，本函数不会返回{@link osKernelError}。
*
* @since 1.0
*/
osKernelState_t osKernelGetState (void);

/**
* @brief 内核启动函数。
*
* 用于开启内核调度，调度之前需要{@link osKernelInitialize}。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误返回{@link osError}。
*
* @since 1.0
*/
osStatus_t osKernelStart (void);

/**
* @brief 内核调度锁定函数。
*
* @return 执行成功返回前一次的锁定状态（1为锁定，0为未锁定）。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误返回{@link osError}。
*
* @since 1.0
*/
int32_t osKernelLock (void);

/**
* @brief 内核调度解锁函数。
*
* @return 执行成功返回前一次的锁定状态（1为锁定，0为未锁定）。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误返回{@link osError}。
*
* @since 1.0
*/
int32_t osKernelUnlock (void);

/**
* @brief 内核调度锁恢复函数。
*
* @param lock 通过{@link osKernelLock或{@link osKernelUnlock}设置的锁定状态。
*
* @return 执行成功返回前一次的锁定状态（1为锁定，0为未锁定）。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误返回{@link osError}。
*
* @since 1.0
*/
int32_t osKernelRestoreLock (int32_t lock);

/**
* @brief 内核挂起函数，暂不支持。
*
* @since 1.0
*/
uint32_t osKernelSuspend (void);

/**
* @brief 内核挂起恢复函数，暂不支持。
*
* @since 1.0
*/
void osKernelResume (uint32_t sleep_ticks);

/**
* @brief 获取内核tick函数。
*
* 用于获取内核当前tick值。
*
* @return 返回当前的tick。
*
* @since 1.0
*/
uint32_t osKernelGetTickCount (void);

/**
* @brief 获取内核tick频率函数。
*
* 用于获取内核每秒tick数。
*
* @return 返回每秒的tick数。
*
* @since 1.0
*/
uint32_t osKernelGetTickFreq (void);

/**
* @brief 获取系统定时器时间。
*
* 获取系统启动后时间。
*
* @return 返回启动后的时间（单位: cycle）。
*
* @since 1.0
*/
uint32_t osKernelGetSysTimerCount (void);

/**
* @brief 获取内核cpu cycle频率函数。
*
* 用于获取内核每秒CPU cycle数。
*
* @return 返回启动后的时间（单位: cycle）。
*
* @since 1.0
*/
uint32_t osKernelGetSysTimerFreq (void);

/**
* @brief 创建线程函数。
*
* 用于创建一个活跃的线程，当创建的线程函数的优先级高于当前的运行线程时 \n
* 创建的线程函数立即启动，成为新的运行线程。
*
* @param func 线程回调入口函数
* @param argument 传递给线程回调函数的参数
* @param attr 线程属性，传递NULL或者对于非NULL结构体中未配置的成员会使用默认值。
* - attr->name 入参为NULL时使用默认名称，若传递非NULL时需确保传递常量。
* - attr->attr_bits 未设置为{@link osThreadJoinable}时会默认设置为{@link osThreadDetached}。
* - attr->cb_mem 暂不支持。
* - attr->cb_size 暂不支持。
* - attr->stack_mem和attr->stack_size同时设置有效数据才能使用静态栈。
* - attr->stack_size 设置为0会使用默认栈大小。
* - attr->priority 当前可用优先级最高为{@link osPriorityAboveNormal1}，最低为{@link osPriorityLow}。
* - attr->tz_module 当前不支持配置。
*
* @return 返回创建的线程ID。
* @return 执行失败或在中断中调用该函数返回NULL。
*
* @since 1.0
*/
osThreadId_t osThreadNew (osThreadFunc_t func, void *argument, const osThreadAttr_t *attr);

/**
* @brief 获取线程名称函数。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 返回线程名称。
* @return 失败或在中断中调用该函数返回NULL。
*
* @since 1.0
*/
const char *osThreadGetName (osThreadId_t thread_id);

/**
* @brief 获取线程ID函数。
*
* 用于获取当前运行线程的ID。
*
* @return 返回线程ID，该函数不会返回失败。
*
* @since 1.0
*/
osThreadId_t osThreadGetId (void);

/**
* @brief 获取线程状态函数。
*
* 用于获取指定线程的状态。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 返回线程状态枚举。
* @return 在中断中调用该函数会返回{@link osThreadError}。
*
* @since 1.0
*/
osThreadState_t osThreadGetState (osThreadId_t thread_id);

/**
* @brief 获取线程栈大小函数。
*
* 用于获取指定线程的栈大小。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 返回指定线程栈大小，单位: 字节。
* @return 失败或在中断中调用该函数会返回0。
*
* @since 1.0
*/
uint32_t osThreadGetStackSize (osThreadId_t thread_id);

/**
* @brief 获取线程栈使用水线函数。
*
* 用于获取指定线程的栈使用水线，该函数无法通过配置来返回栈剩余空间。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 返回指定线程栈水线，单位: 字节。
* @return 失败或在中断中调用该函数会返回0。
*
* 本函数
*
* @since 1.0
*/
uint32_t osThreadGetStackSpace (osThreadId_t thread_id);

/**
* @brief 设置线程优先级函数。
*
* 用于设置线程的优先级。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
* @param priority 待设置的优先级，当前可用优先级最高为{@link osPriorityAboveNormal1}，最低为{@link osPriorityLow}。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 对未创建的线程进行设置返回{@link osErrorResource}。
* @return 参数错误返回{@link osErrorParameter}。
*
* @since 1.0
*/
osStatus_t osThreadSetPriority (osThreadId_t thread_id, osPriority_t priority);

/**
* @brief 获取线程优先级函数。
*
* 用于获取指定线程的优先级。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 成功返回有效的线程优先级。
* @return 参数错误或在中断中调用本函数返回{@link osPriorityError}。
*
* @since 1.0
*/
osPriority_t osThreadGetPriority (osThreadId_t thread_id);

/**
* @brief 线程调度函数。
*
* 用于将控制权让给其他线程，由于本线程未设置为阻塞态，若无其他同优先级可执行线程，可能继续执行本线程。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误或执行错误返回{@link osError}。
*
* @since 1.0
*/
osStatus_t osThreadYield (void);

/**
* @brief 线程挂起函数。
*
* 用于把指定线程设置为挂起状态。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 线程未创建或已挂起或已锁定返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osThreadSuspend (osThreadId_t thread_id);

/**
* @brief 线程挂起恢复函数。
*
* 用于把指定线程设置为就绪状态。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 线程未创建或未挂起返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osThreadResume (osThreadId_t thread_id);

/**
* @brief 线程分离函数。
*
* 用于把指定线程属性设置为{@link osThreadDetached}。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osThreadDetach (osThreadId_t thread_id);

/**
* @brief 等待指定线程终止函数。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osThreadJoin (osThreadId_t thread_id);

/**
* @brief 当前线程退出函数。
*
* 用于本线程退出，本函数不会返回。
*
* @return 本函数不会返回。
*
* @since 1.0
*/
__NO_RETURN void osThreadExit (void);

/**
* @brief 指定线程终止函数。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 线程未创建返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osThreadTerminate (osThreadId_t thread_id);

/**
* @brief 获取使用中的线程数量。
*
* 用于获取使用中的线程数量。
*
* @return 返回线程数量。
* @return 中断中调用该函数会返回0。
*
* @since 1.0
*/
uint32_t osThreadGetCount (void);

/**
* @brief 获取活动线程枚举，暂不支持。
*
* @since 1.0
*/
uint32_t osThreadEnumerate (osThreadId_t *thread_array, uint32_t array_items);

/**
* @brief 设置线程的指定线程标志。
*
* 用于设置线程的指定线程标志。
*
* @param thread_id 线程ID（由{@link osThreadNew}创建或{@link osThreadGetId}获取）。
* @param flags 设置的标志，第25位不能设置为1，函数会返回参数错误。
*
* @return 成功返回设置后的标志。
* @return 参数错误返回{@link osFlagsErrorParameter}。
* @return 其他问题返回{@link osFlagsErrorResource}。
*
* @since 1.0
*/
uint32_t osThreadFlagsSet (osThreadId_t thread_id, uint32_t flags);

/**
* @brief 清除线程的指定线程标志。
*
* 用于清除当前正在运行的线程的指定线程标志。
*
* @param flags 待清除的标志，线程标志的第25位为错误标志。
*
* @return 成功返回清除前的标志
* @return 在中断中调用本函数返回{@link osFlagsErrorUnknown}
* @return 其他问题返回{@link osFlagsErrorResource}
*
* @since 1.0
*/
uint32_t osThreadFlagsClear (uint32_t flags);

/**
* @brief 获取线程的线程标志。
*
* 用于获取当前正在运行的线程的线程标志。
*
* @return 成功返回当前的标志。
* @return 在中断中调用本函数返回{@link osFlagsErrorUnknown}。
*
* @since 1.0
*/
uint32_t osThreadFlagsGet (void);

/**
* @brief 等待指定线程标志。
*
* 用于等待指定线程标志。
*
* @param flags 等待的标志，第 25 位为错误标志。
* @param options 等待的方式，仅限于{@link osFlagsWaitAny} | {@link osFlagsWaitAll} | {@link osFlagsNoClear}。
* @param timeout 等待超时时间，单位：tick。
*
* @return 成功返回清除前的标志。
* @return 在中断中调用本函数返回{@link osFlagsErrorUnknown}。
* @return flag错误或者options错误返回{@link osFlagsErrorParameter}。
* @return 等待超时返回{@link osFlagsErrorTimeout}。
* @return 其他问题返回{@link osFlagsErrorResource}。
*
* @since 1.0
*/
uint32_t osThreadFlagsWait (uint32_t flags, uint32_t options, uint32_t timeout);

/**
* @brief 等待超时函数。
*
* 用于把本线程被置于挂起状态并立即发生上下文切换。 \n
* 开发者可自行提供硬件延时函数VOID HalDelay(UINT32 ticks)， \n
* 即可在系统未运行时执行本函数。
*
* @param ticks 等待的超时tick数。
* @return 成功返回{@link osOK}。
* @return 延时时间为0返回{@link osErrorParameter}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误返回{@link osError}。
*
* @since 1.0
*/
osStatus_t osDelay (uint32_t ticks);

/**
* @brief 指定到期时间的超时函数。
*
* 用于线程延时直到指定时间。
*
* @param ticks 绝对时间，不能小于当前时间。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 其他错误返回{@link osError}。
*
* @since 1.0
*/
osStatus_t osDelayUntil (uint32_t ticks);

/**
* @brief 定时器创建函数。
*
* 用于创建一个定时器，定时器{@link osTimerStart}启动之前处于停止状态。
*
* @param func 定时器回调函数。
* @param type 定时器类型，仅限于{@link osTimerOnce}或{@link osTimerPeriodic}。
* @param argument 定时器回调函数参数*
* @param attr 定时器属性，当前不支持该参数，参数可为NULL。
*
* @return 返回创建的定时器ID。
* @return 执行失败或在中断中调用该函数返回NULL。
*
* @since 1.0
*/
osTimerId_t osTimerNew (osTimerFunc_t func, osTimerType_t type, void *argument, const osTimerAttr_t *attr);

/**
* @brief 获取定时器名称函数。
*
* @param timer_id 定时器ID（由{@link osTimerNew}创建）。
*
* @return 当前只能返回NULL。
* @since 1.0
*/
const char *osTimerGetName (osTimerId_t timer_id);

/**
* @brief 定时器开始运行函数。
*
* 用于开启定时器计时。
*
* @param timer_id 定时器ID（由{@link osTimerNew}创建）。
* @param ticks 定时器颗粒度。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他问题返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osTimerStart (osTimerId_t timer_id, uint32_t ticks);

/**
* @brief 定时器停止运行函数。
*
* @param timer_id 定时器ID（由{@link osTimerNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他问题返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osTimerStop (osTimerId_t timer_id);

/**
* @brief 定时器状态查询函数。
*
* 用于判断定时器是否在运行。
*
* @param timer_id 定时器ID（由{@link osTimerNew}创建）。
*
* @return 成功返回状态（0为未运行，1为运行）。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 入参为NULL也会返回0。
*
* @since 1.0
*/
uint32_t osTimerIsRunning (osTimerId_t timer_id);

/**
* @brief 定时器删除函数。
*
* 用于删除定时器。
*
* @param timer_id 定时器ID（由{@link osTimerNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 在中断中调用本函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他问题返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osTimerDelete (osTimerId_t timer_id);

/**
* @brief 事件初始化函数。
*
* @param attr 事件参数，当前不支持该参数，参数可为NULL。
*
* @return 返回创建的事件ID，执行失败或在中断中调用该函数返回NULL。
*
* @since 1.0
*/
osEventFlagsId_t osEventFlagsNew (const osEventFlagsAttr_t *attr);

/**
* @brief 获取事件名称函数。
*
* @param ef_id 定时器ID（由{@link osEventFlagsNew}创建）。
*
* @return 当前只能返回NULL。
*
* @since 1.0
*/
const char *osEventFlagsGetName (osEventFlagsId_t ef_id);

/**
* @brief 设置事件标志函数。
*
* @param ef_id 定时器ID（由{@link osEventFlagsNew}创建）。
* @param flags 待设置标志，第25位不能设置为1。
*
* @return 成功返回设置后的标志。
* @return 参数错误返回{@link osFlagsErrorParameter}。
* @return 其他问题返回{@link osFlagsErrorResource}。
*
* @since 1.0
*/
uint32_t osEventFlagsSet (osEventFlagsId_t ef_id, uint32_t flags);

/**
* @brief 清除事件标志函数。
*
* 用于清除指定的事件标志，并返回清除前的事件标志。
*
* @param ef_id 定时器ID（由osEventFlagsNew}创建）。
* @param flags 待清除标志。
*
* @return 成功返回清除前的事件标志。
* @return 参数错误返回{@link osFlagsErrorParameter}。
* @return 其他问题返回{@link osFlagsErrorResource}。
*
* @since 1.0
*/
uint32_t osEventFlagsClear (osEventFlagsId_t ef_id, uint32_t flags);

/**
* @brief 获取事件标志函数。
*
* @param ef_id 定时器ID（由{@link osEventFlagsNew}创建）。
*
* @return 成功返回当前事件标志数值。
* @return 失败返回0.
*
* @since 1.0
*/
uint32_t osEventFlagsGet (osEventFlagsId_t ef_id);

/**
* @brief 事件标志等待函数。
*
* @param ef_id 定时器ID（由{@link osEventFlagsNew}创建）。
* @param flags 等待的标志，第25位不能设置为1。
* @param options 等待设置，仅限于{@link osFlagsWaitAny} | {@linkosFlagsWaitAll} | {@link osFlagsNoClear}。
* @param timeout 超时时间，单位：tick。
*
* @return 成功返回等到的标志，内容与options设置有关。
* @return 参数错误或中断中调用该函数返回{@link osFlagsErrorParameter}。
* @return 超时返回{@link osFlagsErrorTimeout}。
* @return 其他问题返回{@link osFlagsErrorResource}。
*
* @since 1.0
*/
uint32_t osEventFlagsWait (osEventFlagsId_t ef_id, uint32_t flags, uint32_t options, uint32_t timeout);

/**
* @brief 事件删除函数。
*
* 用于删除指定的事件。
*
* @param ef_id 定时器ID（由{@link osEventFlagsNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 参数错误返回{@link osFlagsErrorParameter}。
* @return 中断中调用该函数会返回{@link osErrorISR}。
*
* @since 1.0
*/
osStatus_t osEventFlagsDelete (osEventFlagsId_t ef_id);

/**
* @brief 互斥锁初始化函数。
*
* @param attr 互斥锁参数，当前内部未使用该参数，参数可为NULL。
*
* @return 成功返回互斥锁ID。
* @return 失败或在中断中调用该函数会返回NULL。
*
* @since 1.0
*/
osMutexId_t osMutexNew (const osMutexAttr_t *attr);

/**
* @brief 获取互斥锁名称函数，暂不支持。
*
* @since 1.0
*/
const char *osMutexGetName (osMutexId_t mutex_id);

/**
* @brief 获取互斥锁函数。
*
* 用于获取互斥锁，会阻塞线程运行直到获取互斥锁成功或超时。
*
* @param mutex_id 互斥锁ID（由{@link osMutexNew}创建）。
* @param timeout 互斥锁获取超时时间，单位：tick。
*
* @return 成功返回{@link osOK}。
* @return 中断中调用该函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 超时错误返回{@link osErrorTimeout}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMutexAcquire (osMutexId_t mutex_id, uint32_t timeout);

/**
* @brief 释放互斥锁函数。
*
* 用于释放互锁锁，等待该互斥锁的线程会从阻塞中恢复。
*
* @param mutex_id 互斥锁ID（由{@link osMutexNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 中断中调用该函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMutexRelease (osMutexId_t mutex_id);

/**
* @brief 获取互斥锁所有者函数。
*
* @param mutex_id 互斥锁ID（由{@link osMutexNew}创建）。
*
* @return 成功返回线程ID。
* @return 参数错误或中断中调用该函数返回NULL。
*
* @since 1.0
*/
osThreadId_t osMutexGetOwner (osMutexId_t mutex_id);

/**
* @brief 互斥锁删除函数。
*
* 用于删除指定互斥锁。
*
* @param mutex_id 互斥锁ID（由{@link osMutexNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 中断中调用该函数返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMutexDelete (osMutexId_t mutex_id);

/**
* @brief 信号量初始化函数。
*
* 用于初始化信号量。
*
* @param max_count 信号量可申请最大值，max_count值为1时会创建一个二元信号量。
* @param initial_count 信号量初始值。
* @param attr 信号量参数，当前内部未使用该参数，参数可为NULL。
*
* @return 成功返回信号量ID。
* @return 参数错误或中断中调用该函数返回NULL。
*
* @since 1.0
*/
osSemaphoreId_t osSemaphoreNew (uint32_t max_count, uint32_t initial_count, const osSemaphoreAttr_t *attr);

/**
* @brief 获取信号量名称函数。
*
* @param semaphore_id 信号量ID（由{@link osSemaphoreNew}创建）。
*
* @return 当前只能返回NULL。
*
* @since 1.0
*/
const char *osSemaphoreGetName (osSemaphoreId_t semaphore_id);

/**
* @brief 获取信号量函数。
*
* 用于初始化信号量，会阻塞线程运行直到获取信号量成功或超时。
*
* @param semaphore_id 信号量ID（由{@link osSemaphoreNew}创建）。
* @param timeout 超时时间，单位：tick。在中断中调用时必须设置为0。
*
* @return 成功返回{@link osOK}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 超时错误返回{@link osErrorTimeout}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osSemaphoreAcquire (osSemaphoreId_t semaphore_id, uint32_t timeout);

/**
* @brief 释放信号量函数。
*
* 用于释放信号量，等待该信号量的线程会从阻塞中恢复。
*
* @param semaphore_id 信号量ID（由{@link osSemaphoreNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osSemaphoreRelease (osSemaphoreId_t semaphore_id);

/**
* @brief 获取信号量状态量函数。
*
* 用于获取当前的信号量可用数量。
*
* @param semaphore_id 信号量ID（由{@link osSemaphoreNew}创建）。
*
* @return 成功返回当前可用信号量数量，数量可能为0。
* @return 失败返回0。
*
* @since 1.0
*/
uint32_t osSemaphoreGetCount (osSemaphoreId_t semaphore_id);

/**
* @brief 信号量删除函数。
*
* @param semaphore_id 信号量ID（由{@link osSemaphoreNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 参数错误或中断中调用本函数返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osSemaphoreDelete (osSemaphoreId_t semaphore_id);

/**
* @brief 内存池初始化函数。
*
* 用于创建并初始化一个内存池。
*
* @param block_count 内存池块数。
* @param block_size 内存池块大小，单位：字节。
* @param attr 内存池参数，若参数为NULL则使用默认配置。
* - attr->name 暂不支持配置。
* - attr->attr_bits 暂不支持配置。
* - attr->cb_mem不为NULL且attr->cb_size大于控制块所需大小即可使用外部传入的内存池控制块。
*   内存池控制块必需大小当前为36字节（sizeof(MemPoolCB，MemPoolCB定义在cmsis_liteos2.c中）。
* - attr->mp_mem不为NULL且指向的地址符合4字节对即可使用外部传入的内存池。
*
* @return 成功返回内存池ID。
* @return 参数错误或中断中调用本函数返回NULL。
*

*
* @since 1.0
*/
osMemoryPoolId_t osMemoryPoolNew (uint32_t block_count, uint32_t block_size, const osMemoryPoolAttr_t *attr);

/**
* @brief 获取内存池名称函数。
*
* 用于获取内存池名称。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
*
* @return 成功返回内存名。
* @return 参数错误或中断中调用本函数返回NULL。
*
* @since 1.0
*/
const char *osMemoryPoolGetName (osMemoryPoolId_t mp_id);

/**
* @brief 申请内存函数。
*
* 用于从内存池中获取到一块内存，该函数不会阻塞。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
* @param timeout 超时时间，当前不支持阻塞，参数不生效。
*
* @return 成功返回分配内存块的地址。
* @return 参数错误或中断中调用本函数返回NULL。
*
* @since 1.0
*/
void *osMemoryPoolAlloc (osMemoryPoolId_t mp_id, uint32_t timeout);

/**
* @brief 释放内存函数。
*
* 用于从内存池中释放一块内存。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
* @param block 要释放的块地址。
*
* @return 成功返回{@link osOK}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMemoryPoolFree (osMemoryPoolId_t mp_id, void *block);

/**
* @brief 获取内存池拥有块数量函数。
*
* 用于获取初始化的内存池块数量。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
*
* @return 成功返回块数量。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMemoryPoolGetCapacity (osMemoryPoolId_t mp_id);

/**
* @brief 获取内存池块大小函数。
*
* 用于获取初始化的内存池块大小。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
*
* @return 成功返回块大小。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMemoryPoolGetBlockSize (osMemoryPoolId_t mp_id);

/**
* @brief 获取内存池块使用块函数。
*
* 用于获取内存池已使用的块数量。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
*
* @return 成功返回块数量。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMemoryPoolGetCount (osMemoryPoolId_t mp_id);

/**
* @brief 获取内存池剩余块函数。
*
* 用于获取内存池当前剩余的块数量。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
*
* @return 成功返回剩余数量。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMemoryPoolGetSpace (osMemoryPoolId_t mp_id);

/**
* @brief 删除内存池函数。
*
* 用于删除指定的内存池。
*
* @param mp_id 内存池ID（由{@link osMemoryPoolNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 中断中调用该函数会返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMemoryPoolDelete (osMemoryPoolId_t mp_id);


/**
* @brief 消息队列初始化函数。
*
* 用于初始化消息队列，使用外部内存做队列空间时，需提前打开内核编译选项 LOSCFG_BASE_IPC_QUEUE_STATIC。
*
* @param msg_count 队列的节点数量。
* @param msg_size 队列的单个节点空间大小，单位: 字节，使用静态内存做队列空间时该参数不生效但不可设置为0。
* @param attr 队列参数，若参数为NULL则使用默认配置，默认配置时队列使用动态内存。
* - attr->name 入参可为NULL，若传递非NULL时需确保传递一个常量。
* - attr->attr_bits 暂不支持配置。
* - attr->cb_mem 暂不支持配置静态控制块。
* - attr->cb_size 暂不支持配置静态控制块。
* - attr->mq_mem不为NULL且attr->mq_size不为0会使用静态内存做队列空间。
* - attr->mq_mem为NULL且attr->mq_size为0的时候会使用动态内存做队列空间。
* - attr->mq_size需传入整个静态内存的大小，由单个节点大小和节点数量相乘获得。
*
* @return 成功返回消息队列ID。
* @return 参数错误或中断中调用本函数返回NULL。
*
* @since 1.0
*/
osMessageQueueId_t osMessageQueueNew (uint32_t msg_count, uint32_t msg_size, const osMessageQueueAttr_t *attr);

/**
* @brief 获取消息队列名称函数。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
*
* @return 成功返回消息队列名。
* @return 参数错误或未设置队列名返回NULL。
*
* @since 1.0
*/
const char *osMessageQueueGetName (osMessageQueueId_t mq_id);

/**
* @brief 消息队列发送消息函数。
*
* 用于把消息插入队列中，当前不支持消息优先级，新消息会在队列尾部。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
* @param msg_ptr 指向缓冲区的指针，数据会拷贝到消息队列中。
* @param msg_prio 消息优先级，当前不支持该参数。
* @param timeout 超时时间，单位：tick。在中断中调用时必须设置为0。
*
* @return 成功返回{@link osOK}。
* @return 超时错误返回{@link osErrorTimeout}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMessageQueuePut (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout);

/**
* @brief 消息队列接收消息函数。
*
* 用于把消息队列中读取一条消息，当前不支持消息优先级。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
* @param msg_ptr 指向缓冲区的指针，获取到的数据会拷贝到该缓冲区。
* @param msg_prio 消息优先级，当前不支持该参数。
* @param timeout 超时时间，单位：tick。在中断中调用时必须设置为0。
*
* @return 成功返回{@link osOK}。
* @return 超时错误返回{@link osErrorTimeout}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMessageQueueGet (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout);

/**
* @brief 获取消息队列最大节点数函数。
*
* 用于获取指定消息队列最大节点数。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
*
* @return 成功返回队列最大节点数。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMessageQueueGetCapacity (osMessageQueueId_t mq_id);

/**
* @brief 获取消息队列节点大小函数。
*
* 用于获取指定消息队列节点大小，单位: 字节。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
*
* @return 成功返回队列节点大小。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMessageQueueGetMsgSize (osMessageQueueId_t mq_id);

/**
* @brief 获取消息队列待读节点数量函数。
*
* 用于获取消息队列待读节点数量。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
*
* @return 成功返回待读节点数量。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMessageQueueGetCount (osMessageQueueId_t mq_id);

/**
* @brief 获取消息队列剩余可用节点数量函数。
*
* 用于获取消息队列剩余可用读节点数量。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
*
* @return 成功返回剩余可用节点数量。
* @return 参数错误返回0。
*
* @since 1.0
*/
uint32_t osMessageQueueGetSpace (osMessageQueueId_t mq_id);

/**
* @brief 消息队列重置函数，暂不支持。
*
* @since 1.0
*/
osStatus_t osMessageQueueReset (osMessageQueueId_t mq_id);

/**
* @brief 删除消息队列函数。
*
* @param mq_id 消息队列ID（由{@link osMessageQueueNew}创建）。
*
* @return 成功返回{@link osOK}。
* @return 中断中调用该函数会返回{@link osErrorISR}。
* @return 参数错误返回{@link osErrorParameter}。
* @return 其他错误返回{@link osErrorResource}。
*
* @since 1.0
*/
osStatus_t osMessageQueueDelete (osMessageQueueId_t mq_id);


#ifdef  __cplusplus
}
#endif

#endif  // CMSIS_OS2_H_

/** @} */