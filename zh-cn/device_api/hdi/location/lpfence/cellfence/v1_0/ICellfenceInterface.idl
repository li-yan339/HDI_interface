/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceCellfence
 * @{
 *
 * @brief 为低功耗围栏服务提供基站围栏的API。
 *
 * 本模块接口提供添加基站围栏、删除基站围栏和获取基站围栏使用信息的功能。
 *
 * 应用场景：判断用户设备是否达到某个较大范围的位置区域，从而进行一些后续服务，如景区服务介绍等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ICellfenceInterface.idl
 *
 * @brief 声明基站围栏模块提供的API，用于添加基站围栏，删除基站围栏和获取基站围栏使用信息。
 *
 * 模块包路径：ohos.hdi.location.lpfence.cellfence.v1_0
 *
 * 引用：
 * - ohos.hdi.location.lpfence.cellfence.v1_0.CellfenceTypes
 * - ohos.hdi.location.lpfence.cellfence.v1_0.ICellfenceCallback
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.cellfence.v1_0;

import ohos.hdi.location.lpfence.cellfence.v1_0.CellfenceTypes;
import ohos.hdi.location.lpfence.cellfence.v1_0.ICellfenceCallback;

/**
 * @brief 定义对基站围栏模块进行基本操作的接口。
 *
 * 接口包含注册回调函数，取消注册回调函数，添加基站围栏，删除基站围栏和获取基站围栏使用信息。
 *
 * @since 4.0
 * @version 1.0
 */
interface ICellfenceInterface {
    /**
     * @brief 注册回调函数。
     *
     * 用户在开启基站围栏功能前，需要先注册该回调函数。当基站围栏状态发生变化时，会通过回调函数进行上报。
     *
     * @param callbackObj 要注册的回调函数，只需成功订阅一次，无需重复订阅。详见{@link ICellfenceCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RegisterCellfenceCallback([in] ICellfenceCallback callbackObj);

    /**
     * @brief 取消注册回调函数。
     *
     * 取消之前注册的回调函数。当不需要使用基站围栏功能，或需要更换回调函数时，需要取消注册回调函数。
     *
     * @param callbackObj 要取消注册的回调函数，只需成功取消订阅一次，无需重复取消订阅。详见{@link ICellfenceCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UnregisterCellfenceCallback([in] ICellfenceCallback callbackObj);

    /**
     * @brief 添加基站围栏。
     *
     * 支持一次添加多个基站围栏，一个基站围栏包含多个基站信息。
     *
     * @param cellfence 添加的基站围栏信息。详见{@link CellfenceRequest}。
     *
     * @return 如果添加成功，则返回0。
     * @return 如果添加失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    AddCellfences([in] struct CellfenceRequest[] cellfence);

    /**
     * @brief 删除基站围栏。
     *
     * 支持一次删除多个基站围栏。
     *
     * @param cellfenceId 基站围栏ID号。详见{@link CellfenceRequest}。
     *
     * @return 如果删除成功，则返回0。
     * @return 如果删除失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RemoveCellfences([in] int[] cellfenceId);

    /**
     * @brief 获取基站围栏使用信息
     *
     * 查看当前设备支持添加的基站围栏最大个数和已添加的基站围栏个数。通过回调函数上报通知，详见{@link OnGetCellfenceSizeCb}。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetCellfenceSize();
}
/** @} */