/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 模块提供马达服务对马达驱动访问的统一接口，服务获取驱动对象或者代理后，控制马达的单次振动、周期性振动、停止振动、设置马达振幅与频率。
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file VibratorTypes.idl
 *
 * @brief 定义马达数据结构，包括马达振动模式和马达参数。
 *
 * 模块包路径：ohos.hdi.vibrator.v1_1
 *
 * @since 3.2
 * @version 1.1
 */

package ohos.hdi.vibrator.v1_1;

/**
 * @brief 枚举马达的振动模式。
 *
 * @since 2.2
 * @version 1.0
 */
enum HdfVibratorMode {
    HDF_VIBRATOR_MODE_ONCE,     /**< 表示给定持续时间内的单次振动。 */
    HDF_VIBRATOR_MODE_PRESET,   /**< 表示具有预置效果的周期性振动。 */
    HDF_VIBRATOR_MODE_BUTT,     /**< 表示效果模式无效。 */
};

/**
 * @brief 枚举复合效果的效果类型。
 *
 * @since 3.2
 */
enum HdfEffectType {
    /** 表示给定时间序列的时间效果类型 */
    HDF_EFFECT_TYPE_TIME,
    /** 表示给定基本振动序列的基本振动效果类型 */
    HDF_EFFECT_TYPE_PRIMITIVE,
    /** 表示效果类型无效 */
    HDF_EFFECT_TYPE_BUTT,
};

/**
 * @brief 定义马达参数。
 *
 * 参数包括设置马达振幅和频率以及振幅和频率的范围。
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfVibratorInfo {
    int isSupportIntensity;     /**< 设置马达振幅。1表示支持，0表示不支持。 */
    int isSupportFrequency;     /**< 设置马达频率。1表示支持，0表示不支持。 */
    int intensityMaxValue;      /**< 最大振幅。 */
    int intensityMinValue;      /**< 最小振幅。 */
    int frequencyMaxValue;      /**< 最大频率。 */
    int frequencyMinValue;      /**< 最小频率。 */
};

/**
 * @brief 定义时间效果参数。
 *
 * 参数包括振动的延迟、时间、强度和频率。
 *
 * @since 3.2
 */
struct TimeEffect {
    int delay;                   /** 等待时间 */
    int time;                    /** 振动时间 */
    unsigned short intensity;    /** 振动强度 */
    short frequency;             /** 振动频率（Hz） */
};

/**
 * @brief 定义基本效果参数。
 *
 * 参数包括延迟、效应id和振动强度。
 *
 * @since 3.2
 */
struct PrimitiveEffect {
    int delay;                   /** 等待时间 */
    int effectId;                /** 效果id */
    unsigned short intensity;    /** 振动强度 */
};

/**
 * @brief 定义复合效果定义两种效果。
 *
 * 参数包括时间效果和基元效果。
 *
 * @since 3.2
 */
union CompositeEffect {
    struct TimeEffect timeEffect;              /** 时间效果，请参阅{@link TimeEffect}。 */
    struct PrimitiveEffect primitiveEffect;    /** 预定义效果，请参见{@link PrimitiveEffect}。 */
};

/**
 * @brief 定义复合振动效果参数。
 *
 * 参数包括复合效果的类型和顺序。
 *
 * @since 3.2
 */
struct HdfCompositeEffect {
    /** 复合效果的类型，请参见{@link union HdfEffectType}。 */
    int type;
    /** 合成效果的序列，请参见{@link union CompositeEffect}。 */
    union CompositeEffect[] compositeEffects;
};

/**
 * @brief 定义振动效果信息。
 *
 * 该信息包括设置效果的能力和效果的振动持续时间。
 *
 * @since 3.2
 */
struct HdfEffectInfo {
    /** 效果的振动持续时间，以毫秒为单位 */
    int duration;
    /** 设置效果能力。1表示支持，0表示不支持 */
    boolean isSupportEffect;
};
/** @} */
