/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief 定义上层WLAN服务的API接口.
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点
 * WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file IWlanInterface.idl
 *
 * @brief 提供API以启用或禁用WLAN热点、扫描热点、连接到WLAN热点或断开与WLAN热点的连接，设置国家代码，并管理网络设备。
 *
 * 模块包路径：ohos.hdi.wlan.v1_2
 *
 * 引用：ohos.hdi.wlan.v1_1.IWlanInterface
 *
 * @since 4.1
 * @version 1.2
 */

package ohos.hdi.wlan.v1_2;

import ohos.hdi.wlan.v1_1.IWlanInterface;

interface IWlanInterface extends ohos.hdi.wlan.v1_1.IWlanInterface {
    /**
     * @brief 获取ap当前带宽。
     *
     * @param ifName 表示网卡(NIC)名称
     * @param bandwidth ap电流带宽, 1(20M), 2(40M), 4(80M), 8(160M)
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.2
     */
    GetApBandwidth([in] String ifName, [out] unsigned char bandwidth);

    /**
     * @brief 重置为出厂mac地址(永久硬件地址)。
     *
     * @param ifName 表示网卡(NIC)名称
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.2
     */
    ResetToFactoryMacAddress([in] String ifName);

    /**
     * @brief 向驱动程序发送动作帧。
     *
     * @param ifName 表示网卡(NIC)名称
     * @param freq 表示发送通道频率
     * @param ifName 表示动作帧数据
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.2
     */
    SendActionFrame([in] String ifName, [in] unsigned int freq, [in] unsigned char[] frameData);

    /**
     * @brief 寄存器动作帧接收机。
     *
     * @param ifName 表示网卡(NIC)名称
     * @param txChannel 表示数据匹配操作框架
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.2
     */
    RegisterActionFrameReceiver([in] String ifName, [in] unsigned char[] match);

    /**
     * @brief 设置节能管理器模式。
     *
     * @param ifName 表示网卡(NIC)名称
     * @param frequency 表示连接的ap频率
     * @param mode 表示省电模式：3（启用省电），4（禁用省电）
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.2
     */
    SetPowerSaveMode([in] String ifName, [in] int frequency, [in] int mode);

    /**
     * @brief 设置数据包标识标记规则。
     *
     * @param uid 表示目标应用程序uid
     * @param protocol 表示目标协议类型，tcp/udp
     * @param enable 指示启用/禁用dpi标记规则
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.2
     */
    SetDpiMarkRule([in] int uid, [in] int protocol, [in] int enable);
}
/** @} */
