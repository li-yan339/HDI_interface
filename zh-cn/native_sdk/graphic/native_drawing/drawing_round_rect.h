/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_ROUND_RECT_H
#define C_INCLUDE_DRAWING_ROUND_RECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_round_rect.h
 *
 * @brief 文件中定义了与圆角矩形相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_round_rect.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于描述圆角位置的枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_CornerPos {
    /**
     * 左上角圆角位置。
     */
    CORNER_POS_TOP_LEFT,
    /**
     * 右上角圆角位置。
     */
    CORNER_POS_TOP_RIGHT,
    /**
     * 右下角圆角位置。
     */
    CORNER_POS_BOTTOM_RIGHT,
    /**
     * 左下角圆角位置。
     */
    CORNER_POS_BOTTOM_LEFT,
} OH_Drawing_CornerPos;

/**
 * @brief 用于创建一个圆角矩形对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @param xRad X轴上的圆角半径。
 * @param yRad Y轴上的圆角半径。
 * @return 函数会返回一个指针，指针指向创建的圆角矩形对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_RoundRect* OH_Drawing_RoundRectCreate(const OH_Drawing_Rect*, float xRad, float yRad);

/**
 * @brief 用于设置圆角矩形中指定圆角位置的圆角半径。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_RoundRect 指向圆角矩形对象的指针。
 * @param pos 圆角位置的枚举，支持类型可见{@link OH_Drawing_CornerPos}。
 * @param OH_Drawing_Corner_Radii 圆角半径结构体{@link OH_Drawing_Corner_Radii}，其中包含x轴方向和y轴方向上的半径。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RoundRectSetCorner(OH_Drawing_RoundRect*, OH_Drawing_CornerPos pos, OH_Drawing_Corner_Radii);

/**
 * @brief 用于获取圆角矩形中指定圆角位置的圆角半径。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_RoundRect 指向圆角矩形对象的指针。
 * @param pos 圆角位置的枚举，支持类型可见{@link OH_Drawing_CornerPos}。
 * @return 返回指定圆角位置的圆角半径结构体{@link OH_Drawing_Corner_Radii}，其中包含x轴方向和y轴方向上的半径。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Corner_Radii OH_Drawing_RoundRectGetCorner(OH_Drawing_RoundRect*, OH_Drawing_CornerPos pos);

/**
 * @brief 用于销毁圆角矩形对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_RoundRect 指向圆角矩形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_RoundRectDestroy(OH_Drawing_RoundRect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
