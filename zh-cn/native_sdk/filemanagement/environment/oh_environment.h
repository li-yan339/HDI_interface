/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef FILE_MANAGEMENT_ENVIRONMENT_OH_ENVIRONMENT_H
#define FILE_MANAGEMENT_ENVIRONMENT_OH_ENVIRONMENT_H

/**
 * @addtogroup Environment
 *
 * @brief 提供获取公共文件根目录路径的能力。
 * @since 12
 */

/**
 * @file oh_environment.h
 *
 * @brief environment模块接口定义，使用environment提供的native接口，获取公共文件根目录的沙箱路径。
 *
 * @library libohenvironment.so
 * @syscap SystemCapability.FileManagement.File.Environment.FolderObtain
 * @since 12
 */

#include "error_code.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 获取Download根目录沙箱路径。
 *
 * @permission ohos.permission.READ_WRITE_DOWNLOAD_DIRECTORY。
 * @param result Download根目录路径指针。请引用头文件malloc.h并使用free()进行资源释放。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 * @since 12
 */
FileManagement_ErrCode OH_Environment_GetUserDownloadDir(char **result);

/**
 * @brief 获取Desktop根目录沙箱路径。
 *
 * @permission ohos.permission.READ_WRITE_DESKTOP_DIRECTORY。
 * @param result Desktop根目录路径指针。请引用头文件malloc.h并使用free()进行资源释放。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 * @since 12
 */
FileManagement_ErrCode OH_Environment_GetUserDesktopDir(char **result);

/**
 * @brief 获取Document根目录沙箱路径。
 *
 * @permission ohos.permission.READ_WRITE_DOCUMENTS_DIRECTORY
 * @param result Document根目录路径指针。请引用头文件malloc.h并使用free()进行资源释放。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 * @since 12
 */
FileManagement_ErrCode OH_Environment_GetUserDocumentDir(char **result);

#ifdef __cplusplus
};
#endif

#endif // FILE_MANAGEMENT_ENVIRONMENT_OH_ENVIRONMENT_H
